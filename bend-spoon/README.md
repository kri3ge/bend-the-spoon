# **Bend the spoon 💡**
![Authors](https://img.shields.io/badge/authors-1-green)
![Version](https://img.shields.io/badge/version-1.0.0-blue)
![protection](https://img.shields.io/badge/bug_protected-green)
<br><br>
`Bend the spoon` is a simple android quiz app, written on React Native, in which the user has to respond to 10 logical questions in order to bend the spoon completely. 
<br>

If you have any suggestions or issues 🐛, please feel free to share them with me. 💬💬
<br><br>


## **Prerequisites**
Before you begin, se sure you meet the following requirements:
<br>🏁 You have downloaded the `Expo app` on your Android device. You can do it from [this link](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=es&gl=US).
<br>📖 You have read the complete README file.<br><br>
## **Using the app**
To open the `Bend the spoon` app, follow these simple steps:<br>
👀<br>
1.&ensp;Open the project link:
https://expo.io/@krisi/projects/bend-spoon
<br>
2.&ensp;Open the app in your `Expo app` on your device by scanning the QR code from the project link.
<br>
Alternatively, you can open the app in the browser.
Both actions can be performed from this panel
<br><br>
![QR code](./src/Data/readme-img/qr-code.png)
<br>

If you choose to open it in the browser, after loading you will see this page. You can click anywhere to run the app
<br>

![Browser](./src/Data/readme-img/browser-app.png)

3.&ensp;Enjoy the app!

<br>

## **Contact** 🔍

You can reach me at:
<br><kristina.g.zlateva@gmail.com> 

<br>

## **App Preview**

- **Welcome Screen**
<br>

  ![Welcome screen](./src/Data/readme-img/welcome-screen.png)

- **Landing Screen**
<br>

  ![Landing screen](./src/Data/readme-img/landing-screen.png)

- **Info Screen**
<br>

  ![Info screen](./src/Data/readme-img/info-screen.png)

- **Question Screen**
<br>

  ![Question screen](./src/Data/readme-img/question-screen.png)

- **Result Screen**
<br>

  ![Result screen](./src/Data/readme-img/result-screen.png)


