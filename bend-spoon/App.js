import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import React from 'react';
import { Provider } from 'react-redux';
import LandingScreen from './src/Components/LandingScreen';
import WelcomeScreen from './src/Components/WelcomeScreen';
import InfoScreen from './src/Components/InfoScreen';
import QuestionScreen from './src/Components/QuestionScreen';
import ResultScreen from './src/Components/ResultScreen';
import { mainColor } from './src/style/style';
import store from './src/Redux/store';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: mainColor,
            },
            headerTintColor: 'black',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
            headerTitleAlign: 'center',
          }}>
          <Stack.Screen
            name='WelcomeScreen'
            component={WelcomeScreen}
            options={{
              title: null,
              headerStyle: {
                backgroundColor: 'black',
              },
            }}
          />
          <Stack.Screen
            name='LandingScreen'
            component={LandingScreen}
            options={{ title: 'Welcome', headerLeft: null }}
          />
          <Stack.Screen
            name='InfoScreen'
            component={InfoScreen}
            options={{ title: 'Rules of the game' }}
          />
          <Stack.Screen
            name='QuestionScreen'
            component={QuestionScreen}
            options={{ title: 'Question', headerLeft: null }}
          />
          <Stack.Screen
            name='ResultScreen'
            component={ResultScreen}
            options={{ title: 'Your Result', headerLeft: null }}
          />
        </Stack.Navigator>
      </Provider>
    </NavigationContainer>
  );
};

export default App;
