import { StatusBar } from 'expo-status-bar';
import React, { useEffect } from 'react';
import { Text, View, Image, Alert, BackHandler } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { theme } from '../style/style';

const ResultScreen = ({ navigation, route }) => {
  const { score } = route.params;

  useEffect(() => {
    const backAction = () => {
      Alert.alert('Hold on!', 'Are you sure you want to exit the app?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        { text: 'YES', onPress: () => BackHandler.exitApp() },
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const resetGame = () => {
    navigation.navigate('InfoScreen');
  };

  return (
    <View style={theme.container}>
      <Image
        style={theme.einsteinSmall}
        source={require('../Data/einstein.png')}
      />
      <Text style={theme.mainText}>
        {score > 5
          ? `Congrats, your mind is powerful!`
          : score === 0
          ? `Oops! The spoon didn't move at all...`
          : `Not bad, but you'll have to try harder!`}
      </Text>
      <Text style={score > 5 ? theme.scoreTextGood : theme.scoreTextBad}>
        {' '}
        You bent the spoon by {score * 10}%
      </Text>
      <Image
        style={score === 0 ? theme.completeSpoon : theme.bentSpoon}
        source={
          score > 5
            ? require('../Data/bent-100.png')
            : score === 0
            ? require('../Data/bent-0.png')
            : require('../Data/bent-30.png')
        }
      />
      <TouchableOpacity onPress={resetGame}>
        <Text style={theme.bottomText}>Try again</Text>
      </TouchableOpacity>
      <StatusBar style='auto' />
    </View>
  );
};

export default ResultScreen;
