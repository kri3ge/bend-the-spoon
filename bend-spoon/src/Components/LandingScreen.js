import React, { useState } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View, Image, Text } from 'react-native';
import { TextInput, TouchableHighlight } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { theme, mainColor } from '../style/style';

const LandingPage = ({ navigation, dispatch }) => {
  const [username, setUsername] = useState('');

  const registerUser = () => {
    dispatch({ type: 'AUTH', payload: username });
    setUsername('');
    navigation.navigate('InfoScreen');
  };

  return (
    <View style={theme.container}>
      <Text style={theme.mainText}>Write your name and let's get started</Text>
      <Image style={theme.fullSpoon} source={require('../Data/bent-100.png')} />
      <TextInput
        style={theme.mainInput}
        value={username}
        editable
        focused
        maxLength={20}
        placeholder='Your name?'
        onChangeText={(value) => setUsername(value)}
        textAlign='center'
        type='submit'
        onSubmitEditing={registerUser}
      />
      <TouchableHighlight
        disabled={username.length > 0 ? false : true}
        onPress={registerUser}>
        <Ionicons name='md-checkmark-circle' size={32} color={mainColor} />
      </TouchableHighlight>
    </View>
  );
};

export default connect()(LandingPage);
