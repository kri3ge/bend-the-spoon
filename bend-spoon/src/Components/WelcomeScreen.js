import React, { useEffect } from 'react';
import { View, Image, Text, Pressable } from 'react-native';
import { theme } from '../style/style';

const WelcomePage = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('LandingScreen');
    }, 9000);
  });

  const goToLandingScreen = () => {
    navigation.navigate('LandingScreen');
  };

  return (
    <View style={theme.container}>
      <Pressable onPress={goToLandingScreen} style={theme.container}>
        <Text style={theme.mainText}>Welcome to Bend the spoon!</Text>
        <Image
          style={theme.einstein}
          source={require('../Data/einstein.png')}
        />
        <Text style={theme.mainText}>
          Is your mind powerful enough to bend a real spoon?
        </Text>
      </Pressable>
    </View>
  );
};

export default WelcomePage;
