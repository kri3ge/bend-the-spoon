import React, { useEffect, useState } from 'react';
import { Text, View, Image, BackHandler, Alert } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { questions } from '../Data/questionStack';
import { CountDown } from 'react-native-customizable-countdown';
import { theme, mainColor } from '../style/style';

const QuestionScreen = ({ navigation }) => {
  const [questionNumber, setQuestionNumber] = useState(1);
  const [correctAnswers, setCorrectAnswers] = useState(0);

  const currentQuestion = questions.filter(
    (question) => question.id === questionNumber
  )[0];

  useEffect(() => {
    const backAction = () => {
      Alert.alert(
        'Wait a minute!',
        'Are you sure you want to restart the test?',
        [
          {
            text: 'Cancel',
            onPress: () => null,
            style: 'cancel',
          },
          { text: 'Restart', onPress: () => navigation.navigate('InfoScreen') },
        ]
      );
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const handleQuestionsAnswer = (option) => {
    if (option.correct) {
      setCorrectAnswers(correctAnswers + 1);
    }
    if (questionNumber < questions.length) {
      setQuestionNumber(questionNumber + 1);
    } else {
      navigation.navigate('ResultScreen', {
        score: option.correct ? correctAnswers + 1 : correctAnswers,
      });
    }
  };

  const handleFailure = () => {
    navigation.navigate('ResultScreen', {
      score: correctAnswers,
    });
  };

  return (
    <View style={theme.container}>
      <CountDown
        initialSeconds={600}
        onTimeOut={handleFailure}
        showHours={false}
        showMinutes={false}
        digitFontSize={30}
        labelFontSize={15}
        width={200}
        height={70}
        enableLabel={true}
        backgroundColor={mainColor}
        secondsBackgroundStyle={{
          borderWidth: 0,
          backgroundColor: mainColor,
          borderColor: 'white',
        }}
        useNativeDriv
        er={true}
        secondsDigitFontStyle={{ color: 'white' }}
        secondsLabelFontStyle={{ color: 'white' }}
        minutesDigitFontStyle={{ color: 'white' }}
        labelColor='white'
        labelFontWeight='bold'
        labelPosition='top'
      />
      <Text style={theme.mainQuestionText}>{currentQuestion.title}</Text>
      <FlatList
        data={currentQuestion.options}
        renderItem={({ item }) => (
          <TouchableOpacity
            key={item.id}
            onPress={() => handleQuestionsAnswer(item)}
            name={item.correct}>
            <Text style={theme.secondaryQuestionText} name={item.correct}>
              <Image
                style={theme.tinySpoon}
                source={require('../Data/tiny-spoon.png')}
              />{' '}
              {item.text}
            </Text>
          </TouchableOpacity>
        )}
      />
      <Text style={theme.pageText}>{questionNumber}/10</Text>
    </View>
  );
};

export default QuestionScreen;
