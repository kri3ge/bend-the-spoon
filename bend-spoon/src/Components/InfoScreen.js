import React from 'react';
import { Text, View, Image, Pressable } from 'react-native';
import { connect } from 'react-redux';
import { theme } from '../style/style';

const InfoScreen = ({ navigation, userData, dispatch }) => {
  const beginGame = () => {
    navigation.navigate('QuestionScreen');
    dispatch({ type: 'START' });
  };

  return (
    <View style={theme.container}>
      <Text style={theme.mainText}>Pay attention, {userData.username}!</Text>
      <Text style={theme.secondaryText}>
        It's time to answer complicated logical questions.
      </Text>
      <Text style={theme.secondarySmallText}>10 minutes for 10 questions!</Text>
      <Text style={theme.secondarySmallText}>
        First, find out how to start the game...
      </Text>
      <Pressable onPress={beginGame}>
        <Image style={theme.brain} source={require('../Data/brain.png')} />
      </Pressable>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    userData: state.user,
  };
};

export default connect(mapStateToProps)(InfoScreen);
