export const questions = [
    { id: 1, title: 'If you rearrange the letters "CARACTTIN" you have the name of a(n)?', options: [ 
        { id: 1, text: 'Continent', correct: true},
        { id: 2, text: `City`, correct: false},
        { id: 3, text: `Ocean`, correct: false},
        { id: 4, text: `Animal`, correct: false},
    ]},
    { id: 2, title: 'What is the next number in the series: \n 7, 10, 16, 28, 52, ___?', options: [ 
        { id: 1, text: '88', correct: false},
        { id: 2, text: `100`, correct: true},
        { id: 3, text: `66`, correct: false},
        { id: 4, text: `76`, correct: false},
    ]},
    { id: 3, title: 'Which conclusion follows from the statements with absolute certainty? \n None of the runners is a teacher. \n All of the attendees are runners.', options: [ 
        { id: 1, text: 'some drones are teachers', correct: false},
        { id: 2, text: `no runners are attendees`, correct: false},
        { id: 3, text: `teachers are not attendees`, correct: true},
        { id: 4, text: `all runners are teachers`, correct: false},
    ]},
    { id: 4, title: 'What is the missing number in the series: \n 3, 9, 81, 15, 21, 71,  __ , 33, 61?', options: [ 
        { id: 1, text: '56', correct: false},
        { id: 2, text: `22`, correct: false},
        { id: 3, text: `27`, correct: true},
        { id: 4, text: `25`, correct: false},
    ]},
    { id: 5, title: 'A bat and a ball cost $1.10 in total. The bat costs $1 more than the ball. How much does the ball cost?', options: [ 
        { id: 1, text: '10 cents', correct: false},
        { id: 2, text: `5 cents`, correct: true},
        { id: 3, text: `$1`, correct: false},
        { id: 4, text: `20 cents`, correct: false},
    ]},
    { id: 6, title: 'If it takes five machines five minutes to make five widgets, how long would it take 100 machines to make 100 widgets?', options: [ 
        { id: 1, text: '5 minutes', correct: true},
        { id: 2, text: `100 minutes`, correct: false},
        { id: 3, text: `50 minutes`, correct: false},
        { id: 4, text: `10 minutes`, correct: false},
    ]},
    { id: 7, title: 'At a conference, 12 members shook hands with each other before & after the meeting. How many total number of hand shakes occurred?', options: [ 
        { id: 1, text: '100', correct: false},
        { id: 2, text: `132`, correct: true},
        { id: 3, text: `145`, correct: false},
        { id: 4, text: `121`, correct: false},
    ]},
    { id: 8, title: 'The day after the day after tomorrow is four days before Monday. What day is it today?', options: [ 
        { id: 1, text: 'Monday', correct: true},
        { id: 2, text: `Tuesday`, correct: false},
        { id: 3, text: `Wednesday`, correct: false},
        { id: 4, text: `Thursday`, correct: false},
    ]},
    { id: 9, title: '6121135 is to flame as 21215120 is to?', options: [ 
        { id: 1, text: 'voice', correct: false},
        { id: 2, text: `bald`, correct: false},
        { id: 3, text: `bloat`, correct: true},
        { id: 4, text: `castle`, correct: false},
    ]},
    { id: 10, title: 'Kevin, Joseph, and Nicholas are 3 brothers. If the following statements are all true, which of them is the youngest? \n Kevin is the oldest. \n Nicholas is not the oldest. \n Joseph is not the youngest.', options: [ 
        { id: 1, text: 'Joseph', correct: false},
        { id: 2, text: `Kevin`, correct: false},
        { id: 3, text: `Nicholas`, correct: true},
        { id: 4, text: `Both Joseph and Nicholas`, correct: false},
    ]},
]